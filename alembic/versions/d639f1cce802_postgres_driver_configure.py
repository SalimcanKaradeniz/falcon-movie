"""postgres driver configure

Revision ID: d639f1cce802
Revises: 7d194b060a41
Create Date: 2019-04-20 15:07:10.900315

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd639f1cce802'
down_revision = '7d194b060a41'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
