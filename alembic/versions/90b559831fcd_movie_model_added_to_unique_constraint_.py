"""movie model added to unique constraint movie_path_uc

Revision ID: 90b559831fcd
Revises: 8e1c381aaa04
Create Date: 2019-04-21 16:06:35.771418

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '90b559831fcd'
down_revision = '8e1c381aaa04'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
