"""alembic postgre configuration

Revision ID: 7d194b060a41
Revises: 6e0556a736e4
Create Date: 2019-04-20 15:05:01.805447

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7d194b060a41'
down_revision = '6e0556a736e4'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
