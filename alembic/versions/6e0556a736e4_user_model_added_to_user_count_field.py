"""User model added to user_count field

Revision ID: 6e0556a736e4
Revises: 50ae0955e148
Create Date: 2019-04-20 14:59:30.166839

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6e0556a736e4'
down_revision = '50ae0955e148'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
