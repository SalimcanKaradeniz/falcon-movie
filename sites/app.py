__author__ = "Salimcan Karadeniz"
__email__ = "salimcankaradeniz@gmail.com"

from falcon_autocrud.middleware import Middleware
from sites import models
from sites.resources import CategoryListCollectionResource, CategoryCreateCollectionResource, \
    MovieCreateCollectionResource, MovieListCollectionResource, MovieCategoryFilterListResource, \
    UserCreateCollectionResource, MovieFilterCollectionResource, MovieNameFilterCollectionResource, \
    MovieImdbFilterCollectionResource, MovieCategoryTop10ListCollectionResource

import falcon

application = falcon.API(middleware=[Middleware()])
app = application

# User
app.add_route('/user/create/', UserCreateCollectionResource(models.engine))

# Movie
app.add_route('/movie/create/', MovieCreateCollectionResource(models.engine))
app.add_route('/movies/', MovieListCollectionResource(models.engine))
app.add_route('/movie/list/category/', MovieCategoryFilterListResource(models.engine))
app.add_route('/movie/single/', MovieFilterCollectionResource(models.engine))
app.add_route('/movie/name/', MovieNameFilterCollectionResource(models.engine))
app.add_route('/movie/imdb/', MovieImdbFilterCollectionResource(models.engine))
app.add_route('/movie/top10/', MovieCategoryTop10ListCollectionResource(models.engine))

# Category
app.add_route('/category/create/', CategoryCreateCollectionResource(models.engine))
app.add_route('/categories/', CategoryListCollectionResource(models.engine))
