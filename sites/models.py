__author__ = "Salimcan Karadeniz"
__email__ = "salimcankaradeniz@gmail.com"

from sqlalchemy import create_engine, ForeignKey, UniqueConstraint, Index
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, FLOAT, VARCHAR, TEXT
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
# DB_URI = "postgresql+psycopg2://salimcan:salim123@localhost:5432/moviedb"  # Home
DB_URI = "postgresql+psycopg2://postgres:salim@localhost:5432/moviedb"  # Office
engine = create_engine(DB_URI, echo=False, pool_size=10, max_overflow=20)
Session = sessionmaker(bind=engine)


class User(Base):
    __tablename__ = 'user'
    uid = Column(VARCHAR(36), primary_key=True)
    token = Column(VARCHAR(36))
    username = Column(VARCHAR(50))
    password = Column(VARCHAR(200))
    email = Column(VARCHAR(50))
    name = Column(VARCHAR(50))
    surname = Column(VARCHAR(50))
    role = Column(VARCHAR(50))


class Category(Base):
    __tablename__ = 'category'
    __table_args__ = (UniqueConstraint('category_name', name='category_name_uc'),)

    uid = Column(VARCHAR(36), primary_key=True)
    category_name = Column(VARCHAR(50))


class Movie(Base):
    __tablename__ = 'movie'

    uid = Column(VARCHAR(36), primary_key=True)
    category = Column(VARCHAR(36), ForeignKey('category.uid'))
    name = Column(VARCHAR(100))
    path = Column(TEXT)
    description = Column(TEXT)
    image = Column(TEXT)
    movie_type = Column(VARCHAR(100))
    time = Column(VARCHAR(35))
    vision_date = Column(VARCHAR(50))
    quality = Column(VARCHAR(50))
    imdb_score = Column(FLOAT)


def create_index(engine):
    category_index = Index('movie_category_ix', Movie.category).create(engine)
    movie_name_index = Index('movie_name_ix', Movie.name).create(engine)
    movie_type_index = Index('movie_type_ix', Movie.movie_type).create(engine)
    vision_date_index = Index('movie_vision_date_ix', Movie.vision_date).create(engine)
    imdb_index = Index('movie_imdb_ix', Movie.imdb_score).create(engine)


if __name__ == '__main__':
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    create_index(engine)
