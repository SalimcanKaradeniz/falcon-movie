__author__ = "Salimcan Karadeniz"
__email__ = "salimcankaradeniz@gmail.com"

from falcon_autocrud.resource import CollectionResource
from falcon_autocrud.auth import authorize
from falcon_autocrud.resource import authorize as ar
from sites import models
import uuid
import json
import falcon
import hashlib
import secrets


class TokenAuthentication(object):
    def authorize(self, req, resp, resource, params):
        session = models.Session()
        t_query = session.query(models.User).filter_by(role='SysAdmin').first()
        authorizetion = req.get_header('Authorization')
        if authorizetion != t_query.token:
            raise falcon.HTTPForbidden('Permission Denied', 'Token is invalid.', )


def method_authorize(req, resp, resource, params):
    session = models.Session()
    t_query = session.query(models.User).filter_by(role='SysAdmin').first()
    authorizetion = req.get_header('Authorization')
    if authorizetion != t_query.token:
        raise falcon.HTTPForbidden('Permission Denied', 'Token is invalid.', )


@authorize(method_authorize)
class UserCreateCollectionResource(CollectionResource):
    model = models.User
    method = ["POST", ]
    session = models.Session()

    @falcon.before(method_authorize)
    def on_post(self, req, resp, *args, **kwargs):
        doc = req.context["doc"]

        uid = str(uuid.uuid1())
        username = doc["username"]
        password = hashlib.md5(doc["password"].encode()).hexdigest()
        email = doc["email"]
        fullname = doc["fullname"]
        surname = doc["surname"]
        role = doc["role"]

        try:
            q = models.User(
                uid=uid,
                token=secrets.token_bytes(16).hex(),
                username=username,
                password=password,
                email=email,
                name=fullname,
                surname=surname,
                role=role
            )
            self.session.add(q)
            self.session.commit()
        except:
            print("Hata oluştu")
            self.session.rollback()


@authorize(TokenAuthentication)
class CategoryCreateCollectionResource(CollectionResource):
    model = models.Category
    methods = ["POST", ]
    session = models.Session()

    @falcon.before(method_authorize)
    def on_post(self, req, resp, *args, **kwargs):
        uid = str(uuid.uuid1())
        category_name = req.context["doc"]["category_name"]

        try:
            q = models.Category(uid=uid, category_name=category_name)
            self.session.add(q)
            self.session.commit()
        except:
            print("Bu Kategori varmış!!")
            self.session.rollback()


@authorize(TokenAuthentication)
class CategoryListCollectionResource(CollectionResource):
    model = models.Category
    methods = ["GET", ]
    session = models.Session()

    def get(self, req, resp, *args, **kwargs):
        categories = self.session.query(models.Category).all()
        data = []

        if not categories:
            data.append({
                "messages": "Category Not Found"
            })
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({"categories": data})

        for ct in categories:
            data.append({
                "uid": ct.uid,
                "category_name": ct.category_name
            })
            resp.status = falcon.HTTP_200
            resp.body = json.dumps({"categories": data})


@authorize(TokenAuthentication)
class MovieCreateCollectionResource(CollectionResource):
    model = models.Movie
    method = ["POST", ]
    session = models.Session()

    @falcon.before(method_authorize)
    def on_post(self, req, resp, *args, **kwargs):
        doc = req.context["doc"]
        category_name = doc["category"]

        c_id = self.session.query(models.Category).filter_by(category_name=category_name).first()

        movie_name = doc["movie_name"]
        movie_path = doc["movie_path"]
        movie_description = doc["movie_description"]
        movie_img = doc["image"]
        movie_type = doc["movie_type"]
        movie_time = doc["movie_time"]
        vision_date = doc["vision_date"]
        quality = doc["quality"]
        imdb = doc["imdb"]

        try:
            q = models.Movie(
                uid=str(uuid.uuid1()),
                category=c_id.uid,
                name=movie_name,
                path=movie_path,
                description=movie_description,
                image=movie_img,
                movie_type=movie_type,
                time=movie_time,
                vision_date=vision_date,
                quality=quality,
                imdb_score=imdb
            )

            self.session.add(q)
            self.session.commit()
        except:
            print("Bu Film varmış!!!")
            self.session.rollback()


@authorize(TokenAuthentication)
class MovieListCollectionResource(CollectionResource):
    model = models.Movie
    methods = ["GET", ]
    session = models.Session()

    @falcon.before(method_authorize)
    def on_get(self, req, resp, *args, **kwargs):
        movies = self.session.query(models.Movie).all()
        data = []

        if not movies:
            data.append({
                "messages": "Movie Not Found"
            })
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({"movies": data})

        for mv in movies:
            c_name = self.session.query(models.Category).filter_by(uid=mv.category).first()

            data.append({
                'movies': {
                    'uid': mv.uid,
                    "movie_name": mv.name,
                    "category": {
                        "uid": mv.category,
                        "name": c_name.category_name
                    },
                    'movie_path': mv.path,
                    'movie_description': mv.description,
                    'movie_img': mv.image,
                    'movie_type': mv.movie_type,
                    'movie_time': mv.time,
                    'vision_date': mv.vision_date,
                    'quality': mv.quality,
                    'imdb_score': mv.imdb_score,
                }
            })
            resp.status = falcon.HTTP_200
            resp.body = json.dumps({"movies": data})


@authorize(TokenAuthentication)
class MovieCategoryFilterListResource(CollectionResource):
    model = models.Movie
    methods = ["POST", ]

    session = models.Session()
    data_list = []

    @falcon.before(method_authorize)
    def on_post(self, req, resp, *args, **kwargs):
        doc = req.context["doc"]

        ctg_movie = self.session.query(models.Movie).filter_by(category=doc["uid"])
        if ctg_movie is None:
            print("Bu Film yokmuş")
            self.data_list.append({
                "messages": "Movies Not Found"
            })
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({"movies": self.data_list})

            self.session.rollback()

        for cm in ctg_movie:
            c_name = self.session.query(models.Category).filter_by(uid=cm.category).first()

            self.data_list.append({

                'uid': cm.uid,
                "movie_name": cm.name,
                "category": {
                    "uid": cm.category,
                    "name": c_name.category_name
                },
                'movie_path': cm.path,
                'movie_description': cm.description,
                'movie_img': cm.image,
                'movie_type': cm.movie_type,
                'movie_time': cm.time,
                'vision_date': cm.vision_date,
                'quality': cm.quality,
                'imdb_score': cm.imdb_score,

            })
            resp.status = falcon.HTTP_200
            resp.body = json.dumps({'movies': self.data_list})


@authorize(TokenAuthentication)
class MovieFilterCollectionResource(CollectionResource):
    model = models.Movie
    methods = ["POST", ]

    session = models.Session()
    data_list = []

    @falcon.before(method_authorize)
    def on_post(self, req, resp, *args, **kwargs):
        doc = req.context["doc"]

        movie = self.session.query(models.Movie).filter_by(uid=doc["uid"])
        if movie is None:
            print("Bu Film yokmuş")
            self.data_list.append({
                "messages": "Movies Not Found"
            })
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({"movies": self.data_list})

            self.session.rollback()

        for m in movie:
            c_name = self.session.query(models.Category).filter_by(uid=m.category).first()

            self.data_list.append({
                'uid': m.uid,
                "movie_name": m.name,
                "category": {
                    "uid": m.category,
                    "name": c_name.category_name
                },
                'movie_path': m.path,
                'movie_description': m.description,
                'movie_img': m.image,
                'movie_type': m.movie_type,
                'movie_time': m.time,
                'vision_date': m.vision_date,
                'quality': m.quality,
                'imdb_score': m.imdb_score,

            })
            resp.status = falcon.HTTP_200
            resp.body = json.dumps({'movies': self.data_list})


@authorize(TokenAuthentication)
class MovieNameFilterCollectionResource(CollectionResource):
    model = models.Movie
    methods = ["POST", ]

    session = models.Session()
    data_list = []

    @falcon.before(method_authorize)
    def on_post(self, req, resp, *args, **kwargs):
        doc = req.context["doc"]

        movie = self.session.query(models.Movie).filter_by(name=doc["movie_name"])

        if movie is None:
            print("Bu Film yokmuş")
            self.data_list.append({
                "messages": "Movies Not Found"
            })
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({"movies": self.data_list})

            self.session.rollback()

        for m in movie:
            c_name = self.session.query(models.Category).filter_by(uid=m.category).first()

            self.data_list.append({
                'uid': m.uid,
                "movie_name": m.name,
                "category": {
                    "uid": m.category,
                    "name": c_name.category_name
                },
                'movie_path': m.path,
                'movie_description': m.description,
                'movie_img': m.image,
                'movie_type': m.movie_type,
                'movie_time': m.time,
                'vision_date': m.vision_date,
                'quality': m.quality,
                'imdb_score': m.imdb_score,

            })
        resp.status = falcon.HTTP_200
        resp.body = json.dumps({'movies': self.data_list})
        self.data_list = []


@authorize(TokenAuthentication)
class MovieImdbFilterCollectionResource(CollectionResource):
    model = models.Movie
    methods = ["POST", ]

    session = models.Session()
    data_list = []

    @falcon.before(method_authorize)
    def on_post(self, req, resp, *args, **kwargs):
        doc = req.context["doc"]
        movie = self.session.query(models.Movie).filter_by(imdb_score=doc["imdb"])

        if movie is None:
            print("Bu Film yokmuş")
            self.data_list.append({
                "messages": "Movies Not Found"
            })
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({"movies": self.data_list})

            self.session.rollback()

        for m in movie:
            c_name = self.session.query(models.Category).filter_by(uid=m.category).first()

            self.data_list.append({
                'uid': m.uid,
                "movie_name": m.name,
                "category": {
                    "uid": m.category,
                    "name": c_name.category_name
                },
                'movie_path': m.path,
                'movie_description': m.description,
                'movie_img': m.image,
                'movie_type': m.movie_type,
                'movie_time': m.time,
                'vision_date': m.vision_date,
                'quality': m.quality,
                'imdb_score': m.imdb_score,

            })
        resp.status = falcon.HTTP_200
        resp.body = json.dumps({'movies': self.data_list})
        self.data_list = []


@authorize(TokenAuthentication)
class MovieCategoryTop10ListCollectionResource(CollectionResource):
    model = models.Movie
    methods = ["GET", ]

    session = models.Session()
    data_list = []

    @falcon.before(method_authorize)
    def on_get(self, req, resp, *args, **kwargs):
        categories = self.session.query(models.Category).all()

        for ctg in categories:
            movies = self.session.query(models.Movie).filter_by(category=ctg.uid).limit(10)

            for m in movies:
                self.data_list.append({
                    'uid': m.uid,
                    "movie_name": m.name,
                    "category": {
                        "uid": m.category,
                        "name": ctg.category_name
                    },
                    'movie_path': m.path,
                    'movie_description': m.description,
                    'movie_img': m.image,
                    'movie_type': m.movie_type,
                    'movie_time': m.time,
                    'vision_date': m.vision_date,
                    'quality': m.quality,
                    'imdb_score': m.imdb_score,

                })
            resp.status = falcon.HTTP_200
            resp.body = json.dumps({'movies': self.data_list})